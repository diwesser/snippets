#!/bin/bash
# Snippet showing how to remove the directory path from a file path and the
# file's extension with parameter expansion.

basename=$@
basename=${basename##*/}
basename=${basename%.*}
printf "$basename"
