# Merging multiple subtitles into a single video container with subtitle track
# names and without transcoding.

# Tested and working

ffmpeg -i input.mp4 \
  -f srt \
  -i input.srt \
  -i input2.srt \
  -i input3.srt \
  -map 0:0 -map 0:1 -map 1:0 -map 2:0 -map 3:0 \
  -c:v copy -c:a copy -c:s srt -c:s srt -c:s srt \
  -metadata:s:s:0 language=lan \
  -metadata:s:s:1 language=lang2 \
  -metadata:s:s:2 language=lang3 \
  output.mkv


# Tested example

ffmpeg -i The\ Pruitt-Igoe\ Myth.mp4 \
  -f srt \
  -i English.srt \
  -i Spanish.srt \
  -i Arabic.srt \
  -map 0:0 -map 0:1 -map 1:0 -map 2:0 -map 3:0 \
  -c:v copy -c:a copy -c:s srt -c:s srt -c:s srt \
  -metadata:s:s:0 language=English \
  -metadata:s:s:1 language=Spanish \
  -metadata:s:s:2 language=Arabic \
  The\ Pruitt-Igoe\ Myth-subs.mkv


# Untested

ffmpeg -i input.mp4 \
  -f srt \
  -i input.srt \
  -i input2.srt \
  -i input3.srt \
  -map 0:0 -map 0:1 -map 1:0 -map 2:0 -map 3:0 \
  -c:v copy -c:a copy -c:s srt -c:s srt -c:s srt \
  -metadata:s:a:0 title="One"
  -metadata:s:a:0 language=lang
  -metadata:s:a:1 title="Two"
  -metadata:s:a:1 language=lang2
  -metadata:s:s:0 language=lang \
  -metadata:s:s:1 language=lang2 \
  -metadata:s:s:2 language=lang3 \
  output.mkv
